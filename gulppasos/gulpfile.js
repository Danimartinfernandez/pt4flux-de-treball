/*
* Dependencias
*/
var gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify');
  autoprefixer = require('gulp-autoprefixer');
  imagemin = require('gulp-tinypng');
/*
* Configuración de la tarea 'demo'
*/
gulp.task('demo', function () {
  gulp.src('js/source/*')
  .pipe(concat('todo.js'))
  .pipe(uglify())
  .pipe(gulp.dest('build/js'))
});


gulp.task('autoprefix', () =>
    gulp.src('css/*.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('build/css'))
);


gulp.task('imagemin', function () {
    gulp.src('img/*')
        .pipe(imagemin('rlRTYum4eYLceZ6VpEciPgqWgqdAU5Vn'))
        .pipe(gulp.dest('build/img'));
});
